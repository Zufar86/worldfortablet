package com.example.worldfottablet.main.SingIn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.worldfottablet.databinding.ActivitySingInBinding
import com.example.worldfottablet.main.main.MainActivity

class SingInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySingInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySingInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enterButton.setOnClickListener {
            val email = binding.emailText.text.toString()
            val password = binding.passwordText.text.toString()


            if (email.isEmpty())
                binding.emailText.error = "Введите имейл"
            else if (!email.contains("@") && !email.contains("."))
                binding.emailText.error = "Введите корректный иимейл"
            else if (password.isEmpty())
                binding.passwordText.error = "Введите пароль"
            else {
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            }
        }

    }
}